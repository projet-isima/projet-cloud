# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "RG1"
  location = "West Europe"
}


resource "azurerm_recovery_services_vault" "vault" {
  name                = "Backup-Vault"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "Standard"

  soft_delete_enabled = true
}


resource "azurerm_backup_policy_vm" "BP01" {
  name                = "BP01"
  resource_group_name = azurerm_resource_group.rg.name
  recovery_vault_name = azurerm_recovery_services_vault.vault.name

  timezone = "Romance Standard Time" # Equivalent de UTC +1

  instant_restore_retention_days = 1

  backup {
    frequency = "Daily"
    time      = "00:00"
  }

  retention_daily {
    count = 30
  }
 
}

resource "azurerm_backup_policy_vm" "BP02" {
  name                = "BP02"
  resource_group_name = azurerm_resource_group.rg.name
  recovery_vault_name = azurerm_recovery_services_vault.vault.name

  timezone = "Romance Standard Time" # Equivalent de UTC +1

  instant_restore_retention_days = 5

  backup {
    frequency = "Weekly"
    time      = "00:00"
    weekdays = ["Sunday"]
  }


  retention_weekly {
    count    = 8
    weekdays = ["Sunday"]
  }

}



resource "azurerm_storage_account" "storage" {
  name                     = "testbackuptable"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}

resource "azurerm_storage_table" "table" {
  name                 = "vms"
  storage_account_name = azurerm_storage_account.storage.name
}