# coding: utf-8
from azure.identity import ClientSecretCredential
from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.resource import SubscriptionClient
from azure.mgmt.resourcegraph import ResourceGraphClient
from azure.mgmt.recoveryservicesbackup import RecoveryServicesBackupClient
from azure.mgmt.recoveryservicesbackup.models import ProtectedItemResource, AzureIaaSComputeVMProtectedItem
import azure.mgmt.resourcegraph as arg
from azure.common.credentials import ServicePrincipalCredentials
from azure.common import AzureConflictHttpError, AzureMissingResourceHttpError
from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
from msrestazure.azure_exceptions import CloudError
import os

credentials = ServicePrincipalCredentials(
    tenant = os.environ['tenantId'],
    client_id = os.environ["clientId"], 
    secret = os.environ["clientSecret"]
)

credential = ClientSecretCredential(
    client_id = os.environ["clientId"],
    client_secret = os.environ["clientSecret"],
    tenant_id = os.environ["tenantId"]
)

subsClient = SubscriptionClient(
    credential = credential,
    subscription_id = os.environ["subscriptionId"]
)

backup_client = RecoveryServicesBackupClient(
    credentials = credentials,
    subscription_id = os.environ["subscriptionId"]
)

def getresources( strQuery ):
    # Get your credentials from Azure CLI (development only!) and get your subscription list

    subsRaw = []
    for sub in subsClient.subscriptions.list():
        subsRaw.append(sub.as_dict())
    subsList = []
    for sub in subsRaw:
        subsList.append(sub.get('subscription_id'))

    # Create Azure Resource Graph client and set options
    argClient = ResourceGraphClient(credentials)
    argQueryOptions = arg.models.QueryRequestOptions(result_format="objectArray")

    # Create query
    argQuery = arg.models.QueryRequest(subscriptions=subsList, query=strQuery, options=argQueryOptions)

    # Run query
    argResults = argClient.resources(argQuery)

    return argResults.data



#Get all the backup policies
policies = {}
policies_list = getresources('RecoveryServicesResources | where type == "microsoft.recoveryservices/vaults/backuppolicies" | project id,name')
for x in policies_list:
    policies[x['name']] = x['id']



#Get backupable VMs (contain tag BACKUP_POLICY)
strQuery = "Resources | where type =~ 'microsoft.compute/virtualmachines'"
backupables = getresources(strQuery)
vault_name = "Backup-Vault"
fabric_name = "Azure"

account_name = 'testbackuptable'
table_name = 'vms'
table_service = TableService(account_name=account_name, account_key=os.environ['accessKey'])

# Traiter le cas où le tag a été enlevé, enlever la VM de la policy
# Terraform : créer le resource vault dans le code,toutes les backups policies

#Add each VM to its own Backup Policy
for vm in backupables :
    #Check if the VM has the tag BACKUP_POLICY
    vm_name = vm['name'].lower()
    resource_group = vm['resourceGroup']


    try :
        policy_name = vm['tags']['BACKUP_POLICY']
        to_backup = True
    except (KeyError,TypeError) :
        to_backup = False
    
    if to_backup :
        
        # Permet de gérer les vaults dans un autre rg
        # Cas par défaut : utilise le vault dans le meme rg
        try : 
            vault_rg = vm['tags']['VAULT_RESOURCE_GROUP']
        except KeyError:
            print(f"policy par défaut {vm_name}")
            vault_rg = resource_group
        
        policy_name = vm['tags']['BACKUP_POLICY']
        vm_id = vm['id'].lower()
        #print(backup_client.protection_policies.get(vault_name, resource_group, policy_name))

        container_name = f"iaasvmcontainer;iaasvmcontainerv2;{resource_group};{vm_name}".lower()
        protected_item_name = f"VM;iaasvmcontainerv2;{resource_group};{vm_name}"
        
        try :
            protected_item_resource = ProtectedItemResource(
                properties = AzureIaaSComputeVMProtectedItem(
                    policy_id = policies[policy_name].lower(),
                    source_resource_id = vm_id.lower()
                )
            )
            try :
                backup_client.protected_items.create_or_update(
                    vault_name,
                    vault_rg,
                    fabric_name,
                    container_name,
                    protected_item_name,
                    protected_item_resource
                )
            
            except CloudError as e :
                print(f"Resource vault : {vault_name} could not be found in rg : {resource_group}") 
                
            
            elem = {'PartitionKey': resource_group, 'RowKey' : vm['name'], 'VaultResourceGroup' : vault_rg}
        
            try :
                table_service.insert_entity(table_name, elem)
        
            except AzureConflictHttpError :
                table_service.update_entity(table_name, elem)
            
            print(f"Backed_up {vm_name}")
        except KeyError as e :
            print(f"Policy {str(e)} doesn't exist check the tag for vm : {vm_name} in rg : {resource_group}")

    else : 
        print(f"Not backed_up {vm_name}")
        try :
            # Il faut enlever la vm de la backup policy
            task = table_service.get_entity(table_name, resource_group, vm_name)
            resource_group = vm['resourceGroup']
            container_name = f"iaasvmcontainer;iaasvmcontainerv2;{resource_group};{vm_name}".lower()
            protected_item_name = f"VM;iaasvmcontainerv2;{resource_group};{vm_name}"
            
            backup_client.protected_items.delete(
            vault_name,
            resource_group,
            fabric_name,
            container_name,
            protected_item_name,
            )

            table_service.delete_entity(table_name, resource_group, vm_name)
            print(f"Removed from Policy {vm_name}")
        except AzureMissingResourceHttpError :
            pass
            # Rien a faire dans ce cas-la, on ne doit pas backup la vm et elle n'a jamais été backup
