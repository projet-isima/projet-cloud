## Description

Unfortunately, Azure's access rights management can be difficult to manage, in particular having a good tradeoff between letting the developers having the freedom to choose the way they backup their VMs and having decent security standards can be challenging.
There is currently no simple way of letting a person have access to a Backup Vault to change only his own VMs' Backup Policy. 

This project aims at automating Microsoft Azure VM backups, letting developers choose which backup policy they want without having access to the Backup Vault, using VM tags.

## How it works

Using an automated pipeline and a service principal, we go through all the VMs in a given resource group/subscription and look for a tags :

    - BACKUP_POLICY, which tells us which policy to assign to the VM.
    - VAULT_RESOURCE_GROUP, that contains the resource group of the Vault we want to use to backup this specific VM.

We then store VM backup information using Table Storage, this means that we are currently capable of turning off VM Backup by simply removing the tags on the VM.

## How to use

Simply set a few environment variable corresponding to your service principal (which has to have access to the VMs and Backup Vaults).

Then run :
```bash=
pip install -r requirements.txt
python backup.py
```
In our case, we used Gitlab CI's pipelines to automate the job, running everyday at 1 AM and using the .gitlab-ci.yml file. 

Minimal needed infrastructure can be provisioned using our Terraform ```main.tf``` file :

```bash=
terraform apply
```
## Environment variables

Service Principal :
- tenantId 
- clientId
- clientSecret
- subscriptionId

Table Storage:
- accessKey

## Backup Policies

BACKUP_POLICY : BP01
BACKUP_POLICY : BP02
BACKUP_POLICY : BP03

**BP01: daily Backup at midnight** (UTC + 1), 30 days retention

Frequency : Daily  
Time : 00:00  
Timezone : UTC + 1 Paris  
Retain instant recovery snapshot(s) for : 1 day(s)  
Retentation range : 3O day(s)  
Retention of weekly backup point : NO  
Retention of monthly backup point : NO  
Retention of yearly backup point : NO  

**BP02: Weekly Backup (during week-ends), 2 months retention**

Frequency : Weekly  
Days : Sunday  
Time : 00:00  
Timezone : UTC + 1 Paris  
Retain instant recovery snapshot(s) for : 5 day(s)  
Retentation range : 8 week(s)  
Retention of weekly backup point : YES (default)  
Retention of monthly backup point : NO  
Retention of yearly backup point : NO  

**BP03: Monthly Backup, 3 months retention**

Frequency : Weekly  
Days : Wednesday  
Time : 00:00  
Timezone : UTC + 1 Paris  
Retain instant recovery snapshot(s) for : 5 day(s)  
Retentation range : 8 week(s)  
Retention of weekly backup point : YES (default)  
Retention of monthly backup point : NO  
Retention of yearly backup point : NO

## License
[MIT](https://choosealicense.com/licenses/mit/)
